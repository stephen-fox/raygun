// raygun makes it possible to securely write to the standard input of a
// process being debugged by radare2.
//
// raygun can be run in "server" or "client" mode. In server mode, raygun
// listens for incoming connections, and writes data from raygun clients to the
// stdin of a radare2 debuggee. In client mode, users can treat raygun like cat
// or nc by piping data to raygun's stdin, which is sent securely to the
// server instance. For example, `echo 'hello world' | raygun`.
package main

import (
	"bufio"
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"golang.org/x/term"
	"io"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/creack/pty"
)

const (
	appName = "raygun"

	addressArg         = "a"
	notBeforeSlideArg  = "notbeforeslide"
	clientConfigEnvArg = "clientconfigenv"
	verboseLoggingArg  = "v"
	versionArg         = "version"
	examplesArg        = "x"
	helpArg            = "h"

	defaultConfigEnv = "RAYGUN_CLIENT_CONFIG"

	usageFmt = appName + ` - %s

Securely write to the standard input of a process being debugged by radare2.

server mode usage: ` + appName + ` [options] /path/to/r2.cfg

client mode usage: ` + appName + ` [options]

options:
`

	usageExamples = `usage examples:

The application can be run in either client or server mode. Server mode
is invoked by specifying a positional argument that contains the file path
for storing a rarun2 configuration file. Client mode is activated by simply
running the application without any positional arguments. Users send data
to a debuggee by piping to ` + appName + ` in client mode.

> Simple usage example:
# Start the server:
` + appName + ` ~/raygun.r2 &
radare2 -r ~/raygun.r2 -d /usr/bin/cat

# In another shell, set '` + defaultConfigEnv + `' by pasting the client
# configuration blob into 'read' and run the client:
read -s ` + defaultConfigEnv + `
export ` + defaultConfigEnv + `
echo 'hello world' | ` + appName + `

> Allow connections from remote addresses on port 8080:
# Start the server:
` + appName + ` -` + addressArg + ` 0.0.0.0:8080 ~/raygun.r2 &
radare2 -r ~/raygun.r2 -d /usr/bin/cat

# On another computer:
read -s ` + defaultConfigEnv + `
export ` + defaultConfigEnv + `
echo 'hello world' | ` + appName + ` -` + addressArg + ` 192.168.1.2:8080
`
)

var (
	version string
)

// TODO: Maybe support manually writing to pty via stdin?
func main() {
	serverAddr := flag.String(
		addressArg,
		"127.0.0.1:1234",
		"The address to listen on (server mode), or to connect to (client mode)\n")
	x509CertsNotBeforeSlide := flag.Duration(
		notBeforeSlideArg,
		-24*time.Hour,
		"A time.Duration (ex: 300ms, -1.5h, or 2h45m) to slide creation of X.509\n"+
			"certificates by. Useful if the client or server's clocks have drifted\n")
	clientConfigEnv := flag.String(
		clientConfigEnvArg,
		defaultConfigEnv,
		"The name of the environment variable containing the client configuration\n" +
			"JSON (client mode only)")
	verboseLogging := flag.Bool(
		verboseLoggingArg,
		false,
		"Enable verbose logging")
	showVersion := flag.Bool(
		versionArg,
		false,
		"Display the version and exit")
	examples := flag.Bool(
		examplesArg,
		false,
		"Display usage examples")
	help := flag.Bool(
		helpArg,
		false,
		"Display this information")

	flag.Parse()

	if *help {
		fmt.Fprintf(os.Stderr, usageFmt, version)
		flag.PrintDefaults()
		os.Exit(1)
	}

	if *examples {
		os.Stderr.WriteString(usageExamples)
		os.Exit(1)
	}

	if *showVersion {
		os.Stderr.WriteString(version + "\n")
		os.Exit(0)
	}

	if flag.NArg() > 0 {
		server(serverConfig{
			raRunCfgPath:   flag.Arg(0),
			listenAddr:     *serverAddr,
			notBeforeSlide: *x509CertsNotBeforeSlide,
			verboseLogging: *verboseLogging,
		})
	} else {
		client(clientConfig{
			connectAddr: *serverAddr,
			configEnv:   *clientConfigEnv,
		})
	}
}

type clientConfig struct {
	connectAddr string
	configEnv   string
}

func client(config clientConfig) {
	configJSON, isSet := os.LookupEnv(config.configEnv)
	if !isSet {
		log.Fatalf("please set the '%s' environemt variable", config.configEnv)
	}

	var secretConfig clientSecretConfig
	err := json.Unmarshal([]byte(configJSON), &secretConfig)
	if err != nil {
		log.Fatalf("failed to parse client configuration env value as JSON - %s", err)
	}

	privateKeyPEM, err := base64.StdEncoding.DecodeString(secretConfig.PrivateKeyBase64)
	if err != nil {
		log.Fatalf("failed to base64 decode private key pem - %s", err)
	}

	certPEM, err := base64.StdEncoding.DecodeString(secretConfig.CertificateBase64)
	if err != nil {
		log.Fatalf("failed to base64 decode certificate pem - %s", err)
	}

	trustedCertsPEM, err := base64.StdEncoding.DecodeString(secretConfig.TrustedCertsBase64)
	if err != nil {
		log.Fatalf("failed to base64 decode trusted certs pem - %s", err)
	}

	clientCert, err := tls.X509KeyPair(certPEM, privateKeyPEM)
	if err != nil {
		log.Fatalf("failed to create tls.Certificate - %s", err)
	}

	trustedCerts := x509.NewCertPool()
	if !trustedCerts.AppendCertsFromPEM(trustedCertsPEM) {
		log.Fatalln("failed to add trusted cert(s) to cert pool - unknown error")
	}

	conn, err := tls.Dial("tcp", config.connectAddr, &tls.Config{
		ServerName:   secretConfig.ServerName,
		RootCAs:      trustedCerts,
		Certificates: []tls.Certificate{clientCert},
	})
	if err != nil {
		log.Fatalf("failed to dial tls server - %s", err)
	}
	defer conn.Close()

	go func() {
		s := bufio.NewScanner(conn)
		for s.Scan() {
			log.Printf("[server message] %s", s.Text())
		}
	}()

	_, err = io.Copy(conn, os.Stdin)
	if err != nil {
		log.Fatalf("failed to copy stdin into socket - %s", err)
	}
}

type serverConfig struct {
	raRunCfgPath   string
	listenAddr     string
	notBeforeSlide time.Duration
	verboseLogging bool
}

func server(config serverConfig) {
	ca, err := newCACertAndKey(config.notBeforeSlide)
	if err != nil {
		log.Fatalf("failed to create ca certificate and key - %s", err)
	}

	serverTLSCert, sni, err := newServerCertAndKey(ca)
	if err != nil {
		log.Fatalf("failed to create server certificate - %s", err)
	}

	clientSecretConfigJSON, err := newClientConfigBundle(ca, sni)
	if err != nil {
		log.Fatalf("failed to create client config bundle - %s", err)
	}

	certPool := x509.NewCertPool()
	certPool.AddCert(ca.X509Cert)

	l, err := tls.Listen("tcp", config.listenAddr, &tls.Config{
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    certPool,
		Certificates: []tls.Certificate{serverTLSCert},
	})
	if err != nil {
		log.Fatalf("failed to start tls listner - %s", err)
	}

	readPipe, writePipe, err := os.Pipe()
	if err != nil {
		log.Fatalf("failed to create a pipe - %s", err)
	}
	var fromTLSListener io.Reader = readPipe
	if config.verboseLogging {
		fromTLSListener = &verboseReader{
			inner: readPipe,
			name:  "read: conn pipe -> pty",
		}
		log.Println("verbose logging enabled")
	}

	go func() {
		for {
			c, err := l.Accept()
			if err != nil {
				log.Printf("failed to accept connection - %s", err)
				continue
			}

			if config.verboseLogging {
				log.Printf("[%s] new connection", c.RemoteAddr())
			}

			_, err = io.Copy(writePipe, c)
			c.Close()
			if err != nil {
				log.Printf("[%s] socket data copy ended with an error - %s",
					c.RemoteAddr(), err)
			} else if config.verboseLogging {
				log.Printf("[%s] disconnected", c.RemoteAddr())
			}
		}
	}()

	isFirstRun := true
	reloadSignals := make(chan os.Signal)
	signal.Notify(reloadSignals, syscall.SIGUSR1)

	for {
		ioPTY, inputTTY, err := pty.Open()
		if err != nil {
			log.Fatalf("failed to open pty - %s", err)
		}

		// Set PTY into "raw" mode to avoid interpretation of
		// control characters (e.g., '^').
		_, err = term.MakeRaw(int(ioPTY.Fd()))
		if err != nil {
			log.Fatalf("failed to put pty into raw mode - %s", err)
		}

		err = updateOrCreateRarun2ConfigStdio(inputTTY.Name(), config.raRunCfgPath)
		if err != nil {
			log.Fatalf("failed to create or update rarun2 config file at '%s' - %s",
				config.raRunCfgPath, err)
		}

		if isFirstRun {
			log.Printf("client secret config:\n%s", clientSecretConfigJSON)
			isFirstRun = false
		}

		go func() {
			var ioPTYWriter io.Writer = ioPTY
			if config.verboseLogging {
				ioPTYWriter = &verboseWriter{
					inner:  ioPTY,
					name:  "write: conn pipe -> pty",
				}
			}

			_, err := io.Copy(ioPTYWriter, fromTLSListener)
			if err != nil {
				log.Printf("failed to copy data from conn pipe into pty - %s", err)
			}
			readPipe.SetReadDeadline(time.Time{})
		}()

		closed := make(chan struct{})
		go func() {
			_, err = io.Copy(os.Stdout, ioPTY)
			if err != nil {
				log.Printf("cannot write pty output to stdout - %s", err)
			}
			close(closed)
		}()

		select {
		case <-reloadSignals:
		case <-closed:
		}

		// Drain what is in the pipe. Closing the PTY
		// does not end the io.Copy operation, and
		// reading from the pipe is fraught with peril,
		// so this is the way.
		err = readPipe.SetReadDeadline(time.Now().Add(100*time.Millisecond))
		if err != nil {
			log.Printf("set read deadline failed - %s", err)
		}
		ioPTY.Close()
		inputTTY.Close()

		time.Sleep(time.Second)
		log.Printf("creating new pty...")
	}
}

type verboseReader struct {
	inner        io.Reader
	name         string
	printStrings bool
}

func (o verboseReader) Read(p []byte) (n int, err error) {
	log.Printf("[%s] read initiated", o.name)
	n, err = o.inner.Read(p)
	buff := bytes.NewBuffer(nil)
	for i := range p[0:n] {
		buff.WriteString(fmt.Sprintf("[r: byte %d] - 0x%x", i, p[i]))
		if o.printStrings {
			buff.WriteString(fmt.Sprintf(" -> '%c'", p[i]))
		}
		buff.WriteByte('\n')
	}
	log.Printf("[%s] read %d bytes:\n%s", o.name, n, buff.String())
	return n, err
}

type verboseWriter struct {
	inner        io.Writer
	name         string
	printStrings bool
}

func (o verboseWriter) Write(p []byte) (n int, err error) {
	log.Printf("[%s] write initiated", o.name)
	n, err = o.inner.Write(p)
	buff := bytes.NewBuffer(nil)
	for i := range p[0:n] {
		buff.WriteString(fmt.Sprintf("[w: byte %d] - 0x%x", i, p[i]))
		if o.printStrings {
			buff.WriteString(fmt.Sprintf(" -> '%c'", p[i]))
		}
		buff.WriteByte('\n')
	}
	log.Printf("[%s] wrote %d bytes:\n%s", o.name, n, buff.String())
	return n, err
}

func newCACertAndKey(notBeforeSlide time.Duration) (*certAndKeyPair, error) {
	notBefore := time.Now().Add(notBeforeSlide)
	expiresAt := notBefore.Add((24*time.Hour) * 365)
	caTemp, err := x509TemplateWithExpirationOrFail(expiresAt)
	if err != nil {
		return nil, fmt.Errorf("failed to create x509 template - %s", err)
	}
	caTemp.NotBefore = notBefore
	orgNameRaw := make([]byte, 10)
	_, err = rand.Read(orgNameRaw)
	if err != nil {
		return nil, fmt.Errorf("failed to read random data for organization name - %s", err)
	}
	caTemp.Subject.Organization = []string{fmt.Sprintf("%x", orgNameRaw)}
	ca, err := newCertAndKey(caTemp, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create x509 certificate - %s", err)
	}
	return ca, nil
}

func newServerCertAndKey(ca *certAndKeyPair) (tls.Certificate, string, error) {
	serverNameRaw := make([]byte, 10)
	_, err := rand.Read(serverNameRaw)
	if err != nil {
		return tls.Certificate{}, "", fmt.Errorf("failed to generate random server name - %w", err)
	}

	serverEndEntityTemp, err := x509TemplateWithExpirationOrFail(ca.X509Cert.NotAfter)
	if err != nil {
		return tls.Certificate{}, "", fmt.Errorf("failed to create x509 template - %w", err)
	}
	serverEndEntityTemp.NotBefore = ca.X509Cert.NotBefore
	serverName := fmt.Sprintf("%x", serverNameRaw)
	serverEndEntityTemp.Subject.CommonName = serverName
	serverEndEntity, err := newCertAndKey(serverEndEntityTemp, ca)
	if err != nil {
		return tls.Certificate{}, "", fmt.Errorf("failed to create x509 certificate - %w", err)
	}

	tlsCert, err := tls.X509KeyPair(pem.EncodeToMemory(serverEndEntity.CertBlock),
		pem.EncodeToMemory(serverEndEntity.PrivateKeyBlock))
	if err != nil {
		return tls.Certificate{}, "", fmt.Errorf("failed to convert server end-entity to tls.Cert - %w", err)
	}

	return tlsCert, serverName, nil
}

func newClientConfigBundle(ca *certAndKeyPair, sni string) ([]byte, error) {
	if len(sni) == 0 {
		return nil, fmt.Errorf("server name (sni) cannot be empty")
	}
	clientEndEntityTemp, err := x509TemplateWithExpirationOrFail(ca.X509Cert.NotAfter)
	if err != nil {
		return nil, fmt.Errorf("failed to create x509 template - %s", err)
	}
	clientEndEntityTemp.NotBefore = ca.X509Cert.NotBefore
	clientEndEntity, err := newCertAndKey(clientEndEntityTemp, ca)
	if err != nil {
		return nil, fmt.Errorf("failed to create x509 certificate - %s", err)
	}
	return json.Marshal(&clientSecretConfig{
		PrivateKeyBase64:   base64.StdEncoding.EncodeToString(pem.EncodeToMemory(clientEndEntity.PrivateKeyBlock)),
		CertificateBase64:  base64.StdEncoding.EncodeToString(pem.EncodeToMemory(clientEndEntity.CertBlock)),
		TrustedCertsBase64: base64.StdEncoding.EncodeToString(pem.EncodeToMemory(ca.CertBlock)),
		ServerName:         sni,
	})
}

type clientSecretConfig struct {
	PrivateKeyBase64   string `json:"private_key_base64"`
 	CertificateBase64  string `json:"certificate_base64"`
	TrustedCertsBase64 string `json:"trusted_certs_base64"`
	ServerName         string `json:"server_name"`
}

// newCertAndKey creates a new X.509 certificate based upon the provided
// template certificate. See 'x509.CreateCertificate()' for details about
// this functionality. If a non-nil parent certAndKeyPair are provided,
// then the new certificate will be signed by that parent.
func newCertAndKey(template *x509.Certificate, optionalParent *certAndKeyPair) (*certAndKeyPair, error) {
	temp := *template
	templateCopy := &temp

	// ECDSA for P E R F O R M A N C E!
	newCertKey, err := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	if err != nil {
		return nil, fmt.Errorf("failed to generate private key - %s", err.Error())
	}

	var certRaw []byte
	if optionalParent == nil {
		templateCopy.IsCA = true
		templateCopy.BasicConstraintsValid = true
		templateCopy.KeyUsage |= x509.KeyUsageCertSign
		certRaw, err = x509.CreateCertificate(
			rand.Reader,
			templateCopy,
			templateCopy,
			&newCertKey.PublicKey,
			newCertKey)
	} else {
		certRaw, err = x509.CreateCertificate(
			rand.Reader,
			templateCopy,
			optionalParent.X509Cert,
			&newCertKey.PublicKey,
			optionalParent.PrivateKey)
	}
	if err != nil {
		return nil, fmt.Errorf("failed to generate certificate - %s", err.Error())
	}

	// This is unfortunately necessary to get all of the
	// fields set by 'x509.CreateCertificate()'. For example,
	// we do note set Certificate.PublicKey in the "template"
	// certificate given to the 'x509.CreateCertificate()'
	// function because... well, the function takes care of
	// that for us. However, upstream code that needs the
	// Certificate struct also needs the public key to be set.
	// Since there does not appear to be a method to generate
	// both the ASN.1 data, and simultaneously retrieve the
	// parsed Certificate struct - this is what we will do.
	cert, err := x509.ParseCertificate(certRaw)
	if err != nil {
		return nil, fmt.Errorf("failed to parse certificate back from bytes - %s", err.Error())
	}

	privateKeyRaw, err := x509.MarshalECPrivateKey(newCertKey)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal ec private key to bytes - %s", err.Error())
	}

	return &certAndKeyPair{
		X509Cert: cert,
		CertBlock: &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: certRaw,
		},
		PrivateKey: newCertKey,
		PrivateKeyBlock: &pem.Block{
			Type:  "ECDSA PRIVATE KEY",
			Bytes: privateKeyRaw,
		},
	}, nil
}

type certAndKeyPair struct {
	X509Cert        *x509.Certificate
	CertBlock       *pem.Block
	PrivateKey      crypto.Signer
	PrivateKeyBlock *pem.Block
}

func x509TemplateWithExpirationOrFail(expiresAt time.Time) (*x509.Certificate, error) {
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, fmt.Errorf("failed to generate serial number - %s", err.Error())
	}

	return &x509.Certificate{
		SerialNumber: serialNumber,
		NotBefore:    time.Now(),
		NotAfter:     expiresAt,
	}, nil
}

func updateOrCreateRarun2ConfigStdio(ptyPath string, configFilePath string) error {
	existingConfigData, readErr := ioutil.ReadFile(configFilePath)
	if readErr != nil {
		if !os.IsNotExist(readErr) {
			return readErr
		}

		err := os.MkdirAll(filepath.Dir(configFilePath), 0700)
		if err != nil {
			return err
		}
	}

	setStdio := false
	buff := bytes.NewBuffer(nil)
	scanner := bufio.NewScanner(bytes.NewReader(existingConfigData))
	stdioPrefix := []byte("stdio=")
	for scanner.Scan() {
		lineNoLeadingSpaces := bytes.TrimSpace(scanner.Bytes())
		if !setStdio && bytes.HasPrefix(lineNoLeadingSpaces, stdioPrefix) {
			lineLen := len(scanner.Bytes())
			lineNoLeadingSpacesLen := len(lineNoLeadingSpaces)
			if lineLen != lineNoLeadingSpacesLen {
				buff.WriteString(strings.Repeat(" ", lineLen-lineNoLeadingSpacesLen))
			}
			buff.Write(stdioPrefix)
			buff.WriteString(ptyPath)
			setStdio = true
		} else {
			buff.Write(scanner.Bytes())
		}
		buff.WriteByte('\n')
	}

	if !setStdio {
		if len(existingConfigData) == 0 {
			buff.WriteString("#!/usr/bin/rarun2\n")
		}
		buff.WriteString(fmt.Sprintf("%s%s\n", stdioPrefix, ptyPath))
	}

	return ioutil.WriteFile(configFilePath, buff.Bytes(), 0600)
}
