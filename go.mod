module gitlab.com/stephen-fox/raygun

go 1.13

require (
	github.com/creack/pty v1.1.11
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf
)
