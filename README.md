# raygun
raygun makes it possible to securely write to the standard input of a process
being debugged by radare2.

raygun can be run in "server" or "client" mode. In server mode, raygun
listens for incoming connections, and writes data from raygun clients to the
stdin of a radare2 debuggee. In client mode, users can treat raygun like `cat`
or `nc` by piping data to raygun's stdin, which is sent securely to the
server instance. For example, `echo 'hello world' | raygun`.

## Table of contents
[[_TOC_]]

## Features

- Client and server modes utilize mutual TLS authentication. This guarantees
that only authenticated clients can write to stdin
- Safe for use between computers on untrusted (any) networks
- Allows for multiple writes to stdin
- Easy to use

## Usage
Before starting radare2, run raygun in server mode. I like to run it in the
background so that the debuggee's stdout/err appears in the same shell as
radare2. To run raygun in server mode, specify the file path where a rarun2
configuration file should be saved. The path to this file must be provided
to radare2, as it specifies raygun's underlying PTY (refer to the
[How does it work?](#how-does-it-work)) section for more information):

```sh
# Note: This example assumes both instances of raygun will be run
# on the same computer.
raygun ~/raygun.r2 &
radare2 -r ~/raygun.r2 -d /usr/bin/cat
# At this point, you would set some breakpoints and run the 'dc'
# radare2 command to resume execution of 'cat'.
```

raygun will output a JSON blob, which is used to authenticate raygun running
in client mode. This data is secret, and should be handled with care.
Here is an example of the JSON object:

```json
{"private_key_base64":"foo","certificate_base64":"bar","trusted_certs_base64":"boba","server_name":"dbafe578be0f78202383"}
```

By default, raygun listens on 127.0.0.1 (RFC 5735 loopback address), port 1234.
This can be customized with command line options as desired.

In another shell (which could potentially be another computer), set an
environment variable named: `RAYGUN_CLIENT_CONFIG` with the JSON blob as
the value. Ideally, you should do this using `read -s`:

```sh
# Linux.
read -s RAYGUN_CLIENT_CONFIG
# Paste the JSON blob.
export RAYGUN_CLIENT_CONFIG
```

Note that some systems [such as macOS](https://unix.stackexchange.com/a/131221)
have a strict limit on the length of a string that can be provided to programs
like `read`. To work around this, you can use `pbpaste`:

```sh
# macOS.
pbpaste | read RAYGUN_CLIENT_CONFIG
export RAYGUN_CLIENT_CONFIG
```

With the client configuration environment variable set, you can run raygun
by piping data to it:

```sh
echo 'hello world' | raygun
```

This runs raygun in client mode, writing the string `hello world` to the stdin
of the debuggee via the raygun server instance.

## Examples
The following examples can be viewed by running raygun with the `-x` option.

Simple usage example:
```sh
# Start the server:
raygun ~/raygun.r2 &
radare2 -r ~/raygun.r2 -d /usr/bin/cat

# In another shell, set 'RAYGUN_CLIENT_CONFIG' by pasting the client
# configuration blob into 'read' and run the client:
read -s RAYGUN_CLIENT_CONFIG
export RAYGUN_CLIENT_CONFIG
echo 'hello world' | raygun
```

Allow connections from remote addresses on port 8080:
```sh
# Start the server:
raygun -a 0.0.0.0:8080 ~/raygun.r2 &
radare2 -r ~/raygun.r2 -d /usr/bin/cat

# On another computer:
read -s RAYGUN_CLIENT_CONFIG
export RAYGUN_CLIENT_CONFIG
echo 'hello world' | raygun -a 192.168.1.2:8080
```

## Advanced usage
In certain circumstances, it might be necessary to recreate the PTY. raygun
allows users to do this without restarting the server instance by sending
`SIGUSR1` to the server instance. This can be done using `kill`:

```sh
kill -s SIGUSR1 $(pidof raygun)
```

## Use case
The primary use case for this application is securely debugging hacking CTF
challenges that utilize the standard input (stdin) file descriptor.

Interacting with a debuggee's standard file descriptors (namely, stdin) is at
best difficult, and at worst a security risk. The features provided by radare2
are workable, but fall short of expectations when utilized over the network.
For example, radare2 provides a feature to hook up stdin/out to a TCP listener.
It is not possible to change the listen address of this listener, and it
listens on any address. In other words, it accepts connections from addresses
other than loopback. As a result, debugging a vulnerable application (such as
one found in a CTF) incurs a security risk.

radare2 also supports hooking up the standard file descriptors to a PTY,
however this involves managing a configuration file, and manually allocating
a PTY. Under the hood, raygun automates this process.

## How does it work?
Server mode is activated by running the application with a single positional
argument: the file path of a new or existing rarun2 configuration file.

In server mode, the application specifies a rarun2 directive which tells radare2
to hook the application's standard file descriptors to a PTY. The PTY is
automatically created by raygun. This PTY is bolted up to a TLS listener that
requires clients' to submit a certificate that was issued by an in-memory CA.
Once these actions are complete, it writes a client configuration JSON blob to
stdout. This contains the client's certificate, private key, the CA certificate
to trust, and the server name to use in SNI. This data is secret, and should be
handled with care.

Client mode is activated by running the application without any positional
command line arguments. In client mode, the application expects an environment
variable named `RAYGUN_CLIENT_CONFIG` to be set. This must contain the
previously mentioned configuration blob. Once this environment variable is set, 
users can simply pipe data to raygun, which will write it to the debuggee's
stdin via a TLS connection with the raygun server instance. The server instance
writes data from the TLS connection to the PTY, which radare2 connected to the
debuggee.

The client will only connect to a TLS server that offers a certificate issued
by the public portion of the CA contained in the configuration blob.

## Installation
Since this is a Go (Golang) application, the preferred method of installation
is using `go install`. This automates downloading and building Go applications
from source in a secure manner. By default, this copies applications
into `~/go/bin/`.

You must first [install Go](https://golang.org/doc/install). After
installing Go, simply run the following command to install the application:

```sh
go install gitlab.com/stephen-fox/raygun/cmd/raygun@latest
# If successful, the exectuable should be in "~/go/bin/".
```

## Building from source
You may use any of the following methods to build the application from source:

- `go build cmd/raygun/main.go` - Build the application into an executable
- `build.sh` - A simple wrapper around 'go build' that saves build artifacts
  to `build/` and sets a version number in the compiled binary. This script
  expects a version to be provided by setting an environment variable
  named `VERSION`

## Why *raygun*?
[The term](https://codex.uoaf.net/index.php/BVR_tactics#1._RAYGUN_Calls_-_Definition)
is used in beyond visual range air combat as a callout to warn friendly aircraft
that the pilot is targeting an unknown aircraft with radar. If a friendly
aircraft's RWR subsequently indicates a lock, then the pilot should respond with
"buddy spike".
