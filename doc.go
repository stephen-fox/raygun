// Package raygun makes it possible to securely write to the standard input of a
// process being debugged by radare2.
package raygun
